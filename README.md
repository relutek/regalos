# Regalos

La idea de este proyecto es proveer un sistema sencillo para el seguimiento de campañas de donación de regalos.

La importación inicial incluye:

- Una base de datos de la campaña
- Facilidad para importar o ingresar la lista
- Posibilidad de registro de padrinos
- Generación de etiquetas qr
- Página para control de entrega con qr
- Seguimiento de la lista
- Envío de correos

![Scheme](docs/DiagramaRegalos.png)

Front end credo en Vue con el Vue CLI 4
