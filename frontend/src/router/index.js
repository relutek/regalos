import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import Volunteer from "@/views/Volunteer.vue";
import MyGifts from "@/views/MyGifts.vue";
import Login from "@/views/Login.vue";
import Account from "@/views/Account.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/account",
    name: "account",
    component: Account
  },
  {
    path: "/volunteer",
    name: "volunteer",
    component: Volunteer
  },
  {
    path: "/mygifts",
    name: "mygifts",
    component: MyGifts
  }
];

const router = new VueRouter({
  routes
});

export default router;
