const EMAIL_PATTERN = /^[^@]+@[^@]+\.[^@]{2,}$/;
const NAME_PATTERN = /^([a-zA-Záéíóúñ ]){2,}$/;
const PHONE_PATTERN = /^\d{4}[ -]?\d{4}$/;

export function isEmpty(value) {
  if (typeof value === "string") {
    return value.trim().length === 0;
  }

  return value === undefined || value === null;
}

export function isValidEmail(email) {
  return EMAIL_PATTERN.test(email);
}

export function isValidName(name) {
  return NAME_PATTERN.test(name);
}

export function isValidPhone(phone) {
  return PHONE_PATTERN.test(phone);
}

export { validatePassword } from "./password";
