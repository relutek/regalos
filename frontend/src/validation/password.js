const CHAR_CLASSES = [/[a-z]/, /[A-Z]/, /[0-9]/, /[^a-z0-9]/i];
const INVALID_SEQUENCES = [
  /012|123|234|345|456|567|678|789|890/,
  /987|876|765|654|543|432|321/,
  /abc|xyz/i,
  /(.)\1\1/,
  /pass|qwerty/i
];
const SURROUNDING_SPACE = /^\s|\s$/;
const TRANSLATIONS = {
  // warnings
  "Straight rows of keys are easy to guess":
    "Las filas de teclas son fáciles de adivinar",
  "Short keyboard patterns are easy to guess":
    "Patrones cortos de teclado son fáciles de adivinar",
  'Repeats like "aaa" are easy to guess':
    'Repeticiones como "aaa" son fáciles de adivinar',
  'Repeats like "abcabcabc" are only slightly harder to guess than "abc"':
    'Repeticiones como "abcabcabc" son solo un poco más difíciles de adivinar que "abc"',
  "Sequences like abc or 6543 are easy to guess":
    "Secuencias como abc o 6543 son fáciles de adivinar",
  "Recent years are easy to guess": "Los últimos años son fáciles de adivinar",
  "Dates are often easy to guess":
    "Las fechas son a menudo fáciles de adivinar",
  "This is a top-10 common password":
    "Esta contraseña esta en el top-10 de las más comunes",
  "This is a top-100 common password":
    "Esta contraseña esta en el top-100 de las más comunes",
  "This is a very common password": "Esta contraseña es muy común",
  "This is similar to a commonly used password":
    "Esto contraseña es similar a una de uso común",
  "A word by itself is easy to guess": "Una palabra es fácil de adivinar",
  "Names and surnames by themselves are easy to guess":
    "Los nombres y apellidos son fáciles de adivinar",
  "Common names and surnames are easy to guess":
    "Los nombres y apellidos comunes son fáciles de adivinar",
  // suggestions
  "Use a few words, avoid common phrases":
    "Usa algunas palabras, evita frases comunes",
  "No need for symbols, digits, or uppercase letters":
    "No necesita símbolos, dígitos o letras mayúsculas",
  "Add another word or two. Uncommon words are better.":
    "Agregue otra palabra o dos. Las palabras poco comunes son mejores.",
  "Use a longer keyboard pattern with more turns":
    "Use un patrón de teclado más largo con más giros",
  "Avoid repeated words and characters":
    "Evite palabras y caracteres repetidos",
  "Avoid sequences": "Evitar secuencias",
  "Avoid recent years": "Evitar años recientes",
  "Avoid years that are associated with you":
    "Evite los años que están asociados con usted.",
  "Avoid dates and years that are associated with you":
    "Evite fechas y años asociados con usted",
  "Capitalization doesn't help very much": "La capitalización no ayuda mucho",
  "All-uppercase is almost as easy to guess as all-lowercase":
    "Todo en mayúsculas es casi tan fácil de adivinar como todo en minúsculas",
  "Reversed words aren't much harder to guess":
    "Las palabras invertidas no son mucho más difíciles de adivinar",
  "Predictable substitutions like '@' instead of 'a' don't help very much":
    "Las sustituciones predecibles como '@' en lugar de 'a' no ayudan mucho"
};

export function validatePassword(pass, words) {
  let result = {
    score: 0,
    feedback: {
      warning: "",
      suggestions: []
    }
  };

  if (!pass) {
    result.feedback.warning = "Este campo es requerido";
  } else if (SURROUNDING_SPACE.test(pass)) {
    result.feedback.warning = "No se permiten espacios al inicio ni al final";
  } else if (window.zxcvbn) {
    result = window.zxcvbn(pass, words);

    if (result.feedback.warning) {
      result.feedback.warning = TRANSLATIONS[result.feedback.warning];
    }

    if (result.feedback.suggestions) {
      result.feedback.suggestions = result.feedback.suggestions.map(
        s => TRANSLATIONS[s]
      );
    }
  } else {
    // Legacy password validation
    if (pass.length >= 8) {
      result.score += pass.length >= 12 ? 2 : 1;
    } else {
      result.feedback.warning = "Una contraseña muy corta es fácil de romper";
    }

    if (CHAR_CLASSES.filter(c => c.test(pass)).length >= 3) {
      result.score += 1;
    }

    let containsSequences = false;

    for (let seq of INVALID_SEQUENCES) {
      if (seq.test(pass)) {
        containsSequences = true;
        break;
      }
    }

    if (containsSequences) {
      if (result.score <= 2) {
        result.feedback.suggestions.push(
          "Evite secuencias, caracteres repetidos o palabras comunes"
        );
      }
    } else if (result.score > 2) {
      result.score += 1;
    }
  }

  return result;
}
